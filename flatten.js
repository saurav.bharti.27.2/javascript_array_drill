
function recursion(elements){

    let current_size=0;

    for(let index =0 ; index< elements.length; index++){

        if( Array.isArray(elements[index]) ){
            current_size += (recursion(elements[index]))
        }
        else{
            current_size += 1
        }
    }

    return current_size;

}

function flatten(elements){

    return recursion(elements)
}

module.exports = flatten    