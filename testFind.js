
const cb = require('./cb')

const find = require('./find')

const items = [1, 2, 3, 4, 5, 5]

if(!Array.isArray(items)){
    console.log("Kindly, enter a valid array")
    return
}

if((typeof cb)!== 'function'){
    console.log("cb is not a valid function")
    return
}

const result = find(items, cb)
