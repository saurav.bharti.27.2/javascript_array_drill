const map = require('./map.js')

const cb = require('./cb.js') 

const items = [1, 2, 3, 4, 5, 5]

if(!Array.isArray(items)){
    console.log("Kindly enter an array as an input")
    return
}

if((typeof cb)!== 'function'){
    console.log("cb is not a valid function")
    return
}

const result = map(items, cb)