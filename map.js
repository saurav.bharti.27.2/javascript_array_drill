
function map(elements, cb){

    let new_array = []
    
    for(let index =0 ;index<elements.length; index++){
        new_array.push( cb(elements[index], index) )
    }

    return new_array
}

module.exports = map