const reduce = require('./reduce')

const cb = require('./cb')

const items = [1, 2, 3, 4, 5, 5]

if(!Array.isArray(items)){
    console.log("Kindly, enter a valid array")
    return
}

if((typeof cb)!== 'function'){
    console.log("cb is not a valid function")
    return
}


let startingValue

const result = reduce(items, cb, startingValue)