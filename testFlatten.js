
const flatten = require('./flatten')

const nestedArray = [1, [2], [[3]], [[[4]]]]

if(!Array.isArray(nestedArray)){
    console.log("Error, kindly enter a valid array")
    return
}

const result = flatten(nestedArray)