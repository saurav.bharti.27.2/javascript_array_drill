
const cb = require('./cb')

const filter = require('./filter')

const items = [1, 2, 3, 4, 5, 5]

if(!Array.isArray(items)){
    console.log("Kindly, enter a valid array")
    return
}

if((typeof cb)!== 'function'){
    console.log("cb is not a valid function")
    return
}

const result = filter(items, cb)