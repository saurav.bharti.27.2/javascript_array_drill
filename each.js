
function each(elements, cb){

    for(let index =0;index<elements.length; index++){

        elements[index] = cb(elements[index], index)

    }

    return elements
}

module.exports = each